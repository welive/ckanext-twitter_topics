from ckanext.harvest.interfaces import IHarvester
from ckanext.harvest.model import HarvestObject
from ckan.lib.base import c
from ckan.logic import get_action
from ckan import model
from tempfile import NamedTemporaryFile
from ckan.logic.action import create
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import json
import requests
import cgi


class Twitter_TopicsPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(IHarvester)

    config = None

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'twitter_topics')

    # IHarvester

    def info(self):
        return {'name': 'twitter-topic-harvester',
                'title': 'Twitter topic harvester',
                'description': '''A harvester for integrating topics extracted by
                                  Welive\'s Twitter Topic extractor into the
                                  Open Data Stack'''}

    def validate_config(self, config):
        try:
            json_config = json.loads(config)
        except Exception as e:
            raise e
        if 'topic_dataset_id' not in json_config:
            raise Exception("topic_dataset_id not specified")
        else:
            dataset_id = json_config['topic_dataset_id']
            context = {'model': model, 'user': c.user}
            try:
                dataset = get_action('package_show')(context, {'id':
                                                               dataset_id})
            except:
                dataset = []
                pass
            if 'type' in dataset:
                if dataset['type'] != 'dataset':
                    raise Exception("%s can't be used as topic_dataset_id"
                                    % dataset_id)

        if 'language' not in json_config:
            raise Exception("language not specified")

        self.config = json_config
        return config

    def gather_stage(self, harvest_job):
        self.config = json.loads(harvest_job.source.config)

        package_id = self.config.get('topic_dataset_id')
        obj = HarvestObject(guid=package_id, job=harvest_job)
        obj.save()

        return [obj.id]

    def fetch_stage(self, harvest_object):
        self.config = json.loads(harvest_object.job.source.config)

        url = harvest_object.job.source.url
        try:
            content = requests.get(url, verify=False).content
        except ContentFetchError, e:
            self._save_object_error('Unable to get content for package: %s: %r'
                                    % (url, e), harvest_object)
            return False

        harvest_object.content = content
        harvest_object.save()

        return True

    def import_stage(self, harvest_object):
        self.config = json.loads(harvest_object.job.source.config)

        context = {'model': model, 'user': c.user}
        dataset_id = self.config.get('topic_dataset_id')
        dataset = {}
        try:
            dataset = get_action('package_show')(context, {'id': dataset_id})
        except:
            source_dataset = model.Package.get(harvest_object.job.source.id)
            dataset = create.package_create(
                context, {'name': dataset_id,
                          'owner_org': source_dataset.owner_org,
                          'language': self.config.get('language'),
                          'extras': [{'key': 'source',
                                      'value': 'Social Network'}]
                          }
            )

        dataset_object = model.Package.get(dataset['id'])
        harvest_object.package = dataset_object
        f = NamedTemporaryFile(delete=False)
        # f.write(harvest_object.content)

        resource = {}

        if len(dataset['resources']) > 0:
            resource = dataset['resources'][0]
            data = requests.get(resource['url']).json()
            data.append(json.loads(harvest_object.content))
            f.write(json.dumps(data))
            fs = cgi.FieldStorage()
            fs.file = f
            fs.filename = 'topics.json'
            resource['upload'] = fs
            get_action('resource_update')(context, resource)
        else:
            data = [json.loads(harvest_object.content)]
            f.write(json.dumps(data))
            fs = cgi.FieldStorage()
            fs.file = f
            fs.filename = 'topics.json'
            resource = get_action('resource_create')(
                context, {'package_id': dataset['id'],
                          'url': 'topics.json',
                          'upload': fs,
                          'format': 'JSON'}
            )
        f.close()
        harvest_object.current = True
        harvest_object.package_id = dataset['id']
        harvest_object.save()

        return True


class ContentFetchError(Exception):
    pass
